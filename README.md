Website for the Solidare-IT project
===================================

This website is coded in Ruby.

Installation
============

**TODO**

Authors
=======

Matthieu Baerts,
Benoît Baufays,
Julien Colmonts,
Benjamin Frantzen,
Pierre-Yves Légéna,
Vincent Van Ouytsel,
Ludovic Vannoorenberghe,
Alex Vermeylen

License
=======

GPL3
